//
//  RadioType.swift
//  Radio practice
//
//  Created by Akash Balenalli on 10/1/23.
//

import Foundation

/* An enumerated type for views and models to categorize which stream audio data corresponds with. */
enum RadioType {
    case fm
    case digital
}
